#
#http://forasoft.github.io/understanding-ffmpeg/
import os
import sys
import os.path
import logging
import time
import platform
import re
import json
from pathlib import Path
import subprocess
import threading
from multiprocessing.dummy import Pool
from multiprocessing.dummy import Queue
from multiprocessing import cpu_count
import shutil #we have gluster as a destination file system
from loguru import logger
#from trio_inotify.inotify import WatchManager, Watcher, InotifyMasks
#https://habr.com/ru/post/140649/
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class Handler(FileSystemEventHandler):
    # def on_created(self, event):
    #     logger.info(event)

    # def on_deleted(self, event):
    #     logger.info(event)

    # def on_moved(self, event):
    #     logger.info(event)
    def on_modified(self, event):
        global q
        time.sleep(1)
        if not event.is_directory :
            logger.info(f"On modified: {event.src_path}")
            q.put(event.src_path)
            logger.info(f"on_modified q.qsize: {q.qsize()} {q}")
            return event.src_path
        return False
        

H264PATH  = "/var/www/cms/scenes/ftp/cams"
DESTDIR = "/var/www/cms/scenes/ftp/camsimgs/gluster"

hostname = "karopka" #platform.node()
q = Queue()
@logger.catch
def main():
    global H264PATH
    global DESTDIR
    global q
    global hostname
    
    logger.info(hostname)
 
    if not os.path.exists(DESTDIR):
        os.makedirs(DESTDIR)
    h264files=os.listdir(H264PATH)
    logger.info(h264files)

    foundfiles = []
    jpgfiles = []
    h264 = []
    camnames = []
    f2 = os.walk(H264PATH)
    for root, dirs, files in os.walk(H264PATH):
        #logger.info(root,dir,files)
        if files:
            for file in files:
                foundfiles.append(os.path.join(root,file))
        elif not dir:
                os.removedirs(root)
    def findcamname(fname: str, hostname):
        #name = ("".join(re.findall(r'\/\w+_\w+\/', fname))).replace("/", "")
        namesplitted = fname.split("/")
        #logger.info(namesplitted)
        namevariants = []
        for splittedpart in namesplitted:
            foundinsplitted= "".join(re.findall(r'\w*_\w*', splittedpart))
            if foundinsplitted:
                namevariants.append(foundinsplitted)
        name = "_".join(namevariants)
        #logger.info(f"\nFOUND:{namevariants}" )
        camnum = ("".join(re.findall(r'\/\d\d\/', fname))).replace("/", "")
        result = name+'__'+ camnum   
        if hostname :
            result += '__'+ hostname
        result +="_cam_"
        return result
    def getnewname(fname: str, hostname):
        camname = findcamname(fname,hostname)
        fmtime = time.strftime("%Y-%m-%d-%H-%M-%S",time.localtime(os.path.getmtime(fname)))
        return camname + "_" + fmtime

    for fname in foundfiles:
        h264.append(fname)
        camname = findcamname(fname,hostname)
        if not camname in camnames:
            #logger.info(re.findall(r'\/\w+_\w+\/', fname))
            camnames.append(camname)
            #logger.info(fname)
            #logger.info(camname)
            fmtime = time.strftime("%Y-%m-%d-%H-%M-%S",time.localtime(os.path.getmtime(fname)))
            newname = camname + "_" + fmtime
            logger.info(newname)
    logger.info(camnames)

    observer = Observer()
    observer.schedule(Handler(), path=H264PATH, recursive=True)
    observer.start()
    observer.on_thread_start()
    def threadforobserver(observer,q):
        logger.info("Thread for observer started")
        observer.start()
        logger.info("thread for observer queue:{q}")
    def workonfile(filework):
        global H264PATH
        global DESTDIR
        global hostname
#        global q
#        filework = q.get()
        logger.info(f"From workonfile {threading.get_ident()}", filework)
        if not os.path.exists(filework):
            logger.warning(f"File not exists,exiting.: {filework}")
            q.task_done()
            return("File not exists!")
        renamed = getnewname(filework, hostname)
        time.sleep(1)
        if filework.endswith("jpg"):
            newjpgname = os.path.join(DESTDIR,renamed + ".jpg")
            logger.info(f"JPG! moving to {newjpgname}")
            try:
                shutil.move(filework, newjpgname)
            except:
                logger.error(f"Cannot move JPG {filework} {threading.get_ident()}: {sys.exc_info()[0]}")
        elif filework.endswith("h264"):
            renamed = os.path.join(DESTDIR, renamed)
            renamed+=".mp4"
            logger.info(f"h264! {filework} will be renamed to {renamed}")

            #logger.info(renamed)
            #$mp4str="nice -n 15 ffmpeg  -hide_banner -loglevel info -i ".addslashes($f264)." -c:v libx264 ".$newmp4name." -vf \"pad=ceil(iw/2)*2:ceil(ih/2)*2\" 2>&1 ;exit 0";
            #ffmpegcmd = f'nice -n 15 ffmpeg  -hide_banner -loglevel info -i {filework} -c:v libx264 {renamed} -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2\"'
            
            #ffmpegcmd = f'/usr/bin/ffmpeg'
            
            try:
                if os.path.exists(renamed):
                    return("Already out exists!")
                #ffmpegcmd = f'/usr/bin/ffmpeg -f h264 -benchmark -hide_banner -loglevel warning -i {filework} -c:v libx264 {renamed} -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2\" 2>&1'
                ffmpegcmd = f'/usr/bin/ffmpeg -f h264 -benchmark -hide_banner -loglevel warning -y -i {filework} -preset superfast -codec:a libfdk_aac -b:a 128k -codec:v libx264 -pix_fmt yuv420p -b:v 750k -minrate 400k -maxrate 500k -bufsize 1500k -vf scale=-1:360 {renamed} 2>&1'
#                ffmpegcmd = f'/usr/bin/ffmpeg -f h264 -benchmark -hide_banner -loglevel error -i {filework} {renamed}'
                logger.info(f"FFMPEG COMMAND in {threading.get_ident()}:{ffmpegcmd}")
#                ffmpeg_p = subprocess.Popen(ffmpegcmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                ffmpeg_p = subprocess.run(ffmpegcmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                #output, _ = ffmpeg_p.communicate()
                #logger.info(ffmpeg_p.stdout)
                filesize = os.path.getsize(renamed)
                logger.info(f"FFMPEG in {threading.get_ident()} returned: {ffmpeg_p.stdout} Result size: {filesize / 1024 / 1024} MB") 
                os.remove(filework)
            except:
                logger.error(f"FFMPEG: Unexpected error in {ffmpeg_p.stdout} {threading.get_ident()}: {sys.exc_info()[0]}")
                os.remove(filework)
            
        else:
            logger.warning("WTF file ", filework)
        q.task_done()
        return("Done")
        
        
    
    #obsthread = threading.Thread(target=threadforobserver, args=(observer, q,))
    #obsthread.start()

    
    for file in foundfiles:
        if file.endswith("h264") or file.endswith("jpg"):
            q.put(file)
    
    while True:
        time.sleep(10)
        qgot = set()
        pool = Pool(2)
        #logger.info(f"main loop: q.qsize:{q.qsize()}")
        while not q.empty():
            time.sleep(1)
            logger.info(f"loop if not empty: q.qsize:{q.qsize()}")
            #logger.info("Q GET///",)
            #pool = Pool(3)
            qget = None
            qget= q.get()
            logger.info(f"Found in q:{qget}")
            qgot.add(qget)
        if qgot:
            logger.info(f"Adding to queue {len(qgot)}:{qgot}")
            results = pool.map(workonfile, qgot)
            logger.info(f"results:{results}")
            #logger.info(f"Join:{observer.join()}")
        # for event in events:
        #     if 'IN_CLOSE_WRITE' in event[1]:
        #         logger.info("\n Found something")
        #         logger.info(event)
        #logger.info("Getting from queue:",q.get())  
        


if __name__ == '__main__':
    logger.info("Starting main circle... v0.04", q.qsize())
    main()
