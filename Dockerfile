#FROM alpine:3.14
FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive
ENV PATH /usr/local/bin:$PATH
WORKDIR /usr/src/app/
RUN apt-get update -y -q >/dev/null ; echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections >/dev/null  && \
	apt-get -y install python3 python3-pip mc ffmpeg curl wget
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py && \ 
/usr/bin/python3 get-pip.py --force-reinstall
COPY ./app/requirements.txt /tmp/
RUN pip install --requirement /tmp/requirements.txt
#COPY . .
ENTRYPOINT [ "python3", "main.py" ]
#CMD [ "bash -c /usr/bin/sleep", "1000" ]
#ENTRYPOINT [ "sleep", "1000" ]
